import express from 'express';
import IndexController from '../controller/index.controller';
import { validate } from '../utils/api-utils'

import Ajv from 'ajv'


export const indexRouter = function(swaggerSpec: any){

    const router = express.Router();
    const ajv = new Ajv({ strict: false})
    ajv.addSchema(swaggerSpec)

    router.get("/:id", IndexController.getById);

    // use validate middleware to valdate the body of the request usign swagger definitions
    router.patch("/:id", validate('IndexBody', ajv), IndexController.patch);

    return router
}


