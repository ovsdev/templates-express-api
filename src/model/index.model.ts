import express from 'express';
import { dbTojsonData, getSQLQuery, DbField, QueryOptions, query, SQLOptions } from '../utils/sql-utils';
import { HandlerType, getHandler } from '../utils/type-handler'
import BaseModel from './base.model';


class IndexStyle extends BaseModel {

    public constructor(indexMapping: any = {
        "code": new DbField('DB_FIELD_CODE', HandlerType.char25),
        "name": new DbField('DB_FIELD_NAME', HandlerType.char15),
    }) {
        super(indexMapping);
    }

    async getById(req: express.Request) {
        const id = req.params.id;
        const q: QueryOptions = {
            select: { fields: [] },
            from: "TABLE 1 ",
            where: "DB_CODE=?",
            params: [id],
            modelAdapter: this
        }
        const sqlQuery = getSQLQuery(req, q);
        let db2Result: any = await query(req, sqlQuery)
        let result = (db2Result.length > 0) ? await dbTojsonData(db2Result, this) : []

        return result
    }

    async patch(req: express.Request){
        console.log('Unimplemented patch method')
        return
    }


}

export default IndexStyle