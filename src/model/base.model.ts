import { HandlerType, getHandler } from '../utils/type-handler'

import { ModelAdapter } from '../utils/model-adapter';


class BaseModel implements ModelAdapter {

    mapping: any;
    public constructor(mapping: any) {
        this.mapping = mapping
    }

    getDbAttribute(attribute: string): string {
        if (this.mapping[attribute]) {
            return (this.mapping[attribute].tableAlias) ? this.mapping[attribute].tableAlias + '.' + this.mapping[attribute].attr : this.mapping[attribute].attr
        } else {
            throw new Error(`Invalid query parameter \'${attribute}\'`)
        }
    };

    getSelectableFields(): any[] {
        let selectableFields = [] as any
        Object.keys(this.mapping).forEach(k => {
            if (this.mapping[k].selectAllowed) {
                selectableFields.push(k)
            }
        })
        return selectableFields
    }

    getJsonAttribute(attribute: string) {
        const key = Object.keys(this.mapping).filter(k => this.mapping[k].attr == attribute);
        return (key.length == 1) ? key[0] : null
    };

    getJsonAttributeType(attribute: string): HandlerType {
        if (this.mapping[attribute]) {
            return this.mapping[attribute].type
        } else {
            throw new Error(`Invalid query parameter \'${attribute}\'`)
        }
    };

    getMapping(): any {
        return this.mapping
    }

}

export default BaseModel