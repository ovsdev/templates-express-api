import { HandlerType } from'./type-handler'

export interface ModelAdapter{

    getDbAttribute(attribute:string) : string;

    getJsonAttribute(attribute:string) : string;

    getJsonAttributeType(attribute:string) : HandlerType;

    getSelectableFields(): any[]

    getMapping(): any

}
