import { StatusCodes } from 'http-status-codes';
import * as express from 'express'

export class OvsError {
    message: string
    code?: number
    raw?: any

    constructor(message: any, code?: number, raw?: any) {
        this.message = message,
            this.code = code,
            this.raw = raw
    }
}

export function formatError(err: OvsError[] | OvsError) {
    let res: any = { errors: [] }
    if (Array.isArray(err)) {
        err.forEach((e: OvsError) => {
            delete e.code;
            res.errors.push(e)
        })
    } else {
        delete err.code
        res.errors.push(err)
    }
    return res
}

export function formatData(result: any) {
    return { data: result }
}

export function jsonResponse(req: express.Request, res: express.Response, data: any) {
    req.logger.debug('Response data', { response_data: data})
    res.status(StatusCodes.OK).json(formatData(data));
}

export function okResponse(res: express.Response) {
    res.status(StatusCodes.OK).end();
}

export function jsonCreatedResponse(res: express.Response) {
    res.status(StatusCodes.CREATED).end();
}

export function errorResponse(req: express.Request, res: express.Response, error: OvsError[] | OvsError, code?: number) {
    req.logger.error('Error:', error)
    let exitCode: any = code || StatusCodes.BAD_REQUEST
    if (!Array.isArray(error) && error.code) {
        exitCode = error.code
    }
    res.status(exitCode).json(formatError(error));
}

export function unauthorizedResponse(res: express.Response) {
    res.sendStatus(StatusCodes.UNAUTHORIZED);
}

export function unimplementedResponse(res: express.Response) {
    res.status(StatusCodes.NOT_IMPLEMENTED);
}


export const validate = function (swaggerSchema: any, ajv: any) {

    const validatorMiddleware = function (req: express.Request, res: express.Response, next: express.NextFunction) {

        const validator = ajv.validate('#/definitions/' + swaggerSchema, req.body)
        if (validator) {
            next()
        } else {
            req.logger.error('Error on body validation', { request_body_errors: ajv.errors })
            let parsedErrors = ajv.errors.map((e: any) => {
                let field = e.dataPath
                let message = field + ' ' + e.message
                if (e.params.additionalProperty) {
                    message += ' \'' + e.params.additionalProperty + '\''
                }
                return {
                    field,
                    message
                }
            })
            errorResponse(req, res, parsedErrors)
        }
    }
    return validatorMiddleware
}

export function logRequest(req: express.Request, res: express.Response, next: express.NextFunction) {
    let msg = {
        request_body: req.body,
    }
    req.logger.info('Request body details', msg)
    next()
}