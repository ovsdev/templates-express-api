const rql = require('rql/parser');
import { QueryOptions } from './sql-utils'
import { ModelAdapter } from './model-adapter'
import { getHandler } from './type-handler'

interface Operator {
  [key: string]: any;
}

const SqlOp: Operator = {
    eq: '=',
    ge: '>=',
    gt: '>',
    le: '<=',
    lt: '<',
    ne: '!=',
}

function where(sql: any[], root:any, node: { name: string | number; args: any[]; }, modelAdapter : ModelAdapter, values: any[]) {
    switch (node.name) {
        case 'and':
        case 'or':
            const args = node.args && node.args.filter((a:any) => (a.name && !root.cache[a.name]));
            if (args.length > 0) {
                sql.push('(');
                where(sql, root, args[0], modelAdapter, values);
                for (let i = 1; i < args.length; i++) {
                    sql.push(' ');
                    sql.push(node.name);
                    sql.push(' ');
                    where(sql, root, args[i], modelAdapter, values);
                }
                sql.push(')');
            }
            break;
        default:
            if (node.args[1] === 'NULL') {
                sql.push(modelAdapter.getDbAttribute(node.args[0]));
                sql.push(' is ');
                switch (node.name) {
                    case 'eq':
                        break;
                    case 'ne':
                        sql.push('not ');
                        break;
                    default:
                        throw new Error('NULL value can only be tested for equality or inequality');
                }
                sql.push('null');
            }
            else {
                const op = SqlOp[node.name];
                let argType = modelAdapter.getJsonAttributeType(node.args[0]);

                let handler = getHandler(argType);
                let value = handler.jsonToDb2Data(node.args[1])
                // console.log('value: ', value)

                if (op) {
                    sql.push(modelAdapter.getDbAttribute(node.args[0]));
                    sql.push(op);
                    sql.push('?');
                    values.push(value)
                    // sql.push(node.args[1]);
                }else{
                    throw new Error(`Unknown operator ${op}`);
                }
            }
            break;
    }
}

function getWhereClause(node : any , modelAdapter: ModelAdapter) {
    const sql: any[] = [];
    const values: any[] = [];
    where(sql, node, node, modelAdapter, values);

    console.log("sql",sql)
    return {sql: sql.join(''), values};
}

function getQuery(node: any, baseQueryObject: any, modelAdapter : ModelAdapter) {
    const sql = [];
    const cache = node.cache;

    // Where
    const whereClause = getWhereClause(node, modelAdapter);
    if(whereClause || baseQueryObject.where){
        sql.push('where ')
    }
    if(baseQueryObject.where){
        sql.push(baseQueryObject.where)
    }

    if (whereClause) {
        if(baseQueryObject.where){
            sql.push(' and ');
        }
        sql.push(whereClause.sql);
    }

    // Sort
    if (cache.sort) {
        sql.push(' order by ');
        sql.push(cache.sort.map((f: any) => {
            const dir = f[0] === '-' ? ' desc' : '';

            if (f[0] == '+' || f[0] == '-') {
                f = f.substr(1);
            }

            return modelAdapter.getDbAttribute(f) +' '+dir;
        }).join(','));
    }

    // Limit
    if (cache.limit) {
        if (cache.limit[1]) {
            sql.push('offset ');
            sql.push(cache.limit[1]); // TODO: validate
            sql.push(' rows fetch next ');
        }
        else {
            sql.push(' fetch first ');
        }
        sql.push(cache.limit[0]); // TODO: validate
        sql.push(' rows only');
    }

    let selectFields = null
    if(node.cache.select){
        let mappedSelectFields  = node.cache.select.filter( (f:any)=> { return modelAdapter.getDbAttribute(f)!='' && modelAdapter.getDbAttribute(f)!=null })
        selectFields = mappedSelectFields.map((f: any)=>{
            try {
                return modelAdapter.getDbAttribute(f);
            } catch (error) {
                let selectableFields = modelAdapter.getSelectableFields()
                throw new Error(`Invalid select parameter ${f}. Allowed select parameters are: ${selectableFields.join(', ')}`)
            }
        })
    }
    return {
        select: selectFields,
        where: sql.join(''),
        values: whereClause.values
    }
}

// TODO: not for production!!! Need to sanitize query params
export function convertRqlToSql(query: any, baseQueryObject: QueryOptions, modelAdapter: ModelAdapter) {
    const queryAst = rql.parseQuery(query);
    return getQuery( queryAst, baseQueryObject, modelAdapter);
}