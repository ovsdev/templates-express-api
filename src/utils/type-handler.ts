import moment from 'moment'

export enum HandlerType {
    yorn,
    booleanNotNull,
    string,
    number,
    decimal3,
    decimal2,
    decimal6,
    timestamp,
    char1,
    char3,
    char8,
    char7,
    char15,
    char24,
    char25,
    char50,
    isUsable,
    isDropped,
    integer,
    stringToInt
}

interface Handler {
    db2TojsonData(value: any): any,
    jsonToDb2Data(value: any): any,
}

type TypeHandler = {
    [key in keyof typeof HandlerType]?: Handler;
}

const char = function(size: number) {
    let charHandler: Handler = {
        db2TojsonData(value: string) {
            let res = ""+value
            return res.trim()
        },
        jsonToDb2Data(value: string) { return (value==null)? '': value.substr(0, size) }
    }
    return charHandler
}

const decimal = function(nDigit: number){
    let decimalHandler: Handler = {
        db2TojsonData(value: string): number { return parseFloat(value) },
        jsonToDb2Data(value: number): string { return (value==null) ? '0' : value.toFixed(nDigit) }
    }
    return decimalHandler
}


let timestampHandler: Handler = {
    db2TojsonData(value: string): string { return value },
    jsonToDb2Data(value: string): string {
            let time = moment(value)
            let timestamp = (time.isValid()) ? time.format('YYYY-MM-DD HH:mm:ss') : moment().format('YYYY-MM-DD HH:mm:ss')
            return timestamp
    }
}

let yornHandler: Handler = {
    db2TojsonData(value: string): boolean { return value == 'S' },
    jsonToDb2Data(value: boolean): string { return (value) ? 'S' : 'N' }
}

let integerHandler: Handler = {
    db2TojsonData(value: string): number { return parseInt(value,10) },
    jsonToDb2Data(value: number): number { return (value==null)? 0 : value }
}

let stringToInt: Handler = {
    db2TojsonData(value: string): string { return value },
    jsonToDb2Data(value: string): number { return (value==null)? 0 : parseInt(value, 10) }
}

let booleanNotNullHandler: Handler = {
    db2TojsonData(value: string) { return value != null },
    jsonToDb2Data(value: any) { return (value) }
}

let isUsablelHandler: Handler = {
    db2TojsonData(value: string) { return value != null },
    jsonToDb2Data(value: any) { return (value) ? null : moment().format('YYYY-MM-DD HH:mm:ss') }
}

let isDroppedHandler: Handler = {
    db2TojsonData(value: string): boolean { return value != null },
    jsonToDb2Data(value: boolean) { return (!value) ? null : moment().format('YYYY-MM-DD HH:mm:ss') }
}

let defaultHandler: Handler = {
    db2TojsonData(value: any): any { return value },
    jsonToDb2Data(value: any): any { return (value==null)? '': value },
}

const Handlers: TypeHandler = {
    [HandlerType.yorn]: yornHandler,
    [HandlerType.booleanNotNull]: booleanNotNullHandler,
    [HandlerType.char1]: char(1),
    [HandlerType.char3]: char(3),
    [HandlerType.char7]: char(7),
    [HandlerType.char8]: char(8),
    [HandlerType.char15]: char(15),
    [HandlerType.char24]: char(24),
    [HandlerType.char25]: char(25),
    [HandlerType.char50]: char(50),
    [HandlerType.isUsable]: isUsablelHandler,
    [HandlerType.timestamp]: timestampHandler,
    [HandlerType.decimal3]: decimal(3),
    [HandlerType.decimal2]: decimal(2),
    [HandlerType.decimal6]: decimal(6),
    [HandlerType.integer]: integerHandler,
    [HandlerType.isDropped]: isDroppedHandler,
    [HandlerType.stringToInt]: stringToInt
}

export function getHandler(type: HandlerType): Handler {
    return Handlers[type] || defaultHandler
}