import express from 'express';
import { convertRqlToSql } from './rql-parser'
import { getHandler, HandlerType } from './type-handler';
import { ModelAdapter } from './model-adapter';
import { StatusCodes } from 'http-status-codes'

export class QueryOptions {
    select: { fields: string[], distinct?: boolean };
    from: string;
    where: string;
    params: any[]; // ??
    modelAdapter: ModelAdapter;
    ignoreRql?: boolean;
    arraySize?: any; // ??
}

export class SQLQuery {
    sql: string;
    params: any[];
    arraySize?: any;
}

// function db2(attr: string, type: HandlerType, tableAlias: string = '', selectAllowed: boolean = true, required: boolean = false ): any {
export class DbField {
    constructor(public attr: string,
                public type: HandlerType,
                public tableAlias: string = '',
                public selectAllowed: boolean = true,
                public required: boolean = false ) {}
}

export class SQLOptions {
    private params:string[];
    private values:string[];
    private placeholders:string[];

    constructor(){
        this.params = []
        this.placeholders = []
        this.values = []
    }

    prepareOptions(model: ModelAdapter, source: any, suffix?: string){
        Object.keys(source).forEach(k => {
            let attr = (suffix) ? suffix+k: k;
            if (model.getDbAttribute(attr)) {
                this.params.push(model.getDbAttribute(attr))
                let mapping = model.getMapping()
                let handler = getHandler(mapping[attr].type)
                let db2Value = handler.jsonToDb2Data(source[k]);
                this.values.push(db2Value)
                this.placeholders.push('?')
            }
        })
    }

    getParams(){
        return this.params;
    }
    getValues(){
        return this.values;
    }
    getPlaceholders(){
        return this.placeholders;
    }

    pushValue(value: any){
        this.values.push(value)
    }

    addParam(param: string, value?: string, placeholder?: string){
        this.params.push(param)
        if(value)
            this.values.push(value)
        if(placeholder){
            this.placeholders.push(placeholder)
        }
    }

}


export function getSQLQuery(req: express.Request, baseQueryOptions: QueryOptions) {
    const modelAdapter = baseQueryOptions.modelAdapter;

    const mapping = modelAdapter.getMapping();
    const requiredAttributes = Object.keys(mapping).filter( (f : any) =>  { return mapping[f].required } )
    const requiredDbFields = requiredAttributes.map( f => { return modelAdapter.getDbAttribute(f)})
    const dbQuery: SQLQuery = {
        sql: ``,
        params: baseQueryOptions.params
    };
    const rqlQuery = (req._parsedOriginalUrl )? req._parsedOriginalUrl.query: null

    // add required db fields
    if (baseQueryOptions.select && baseQueryOptions.select.fields) {
        requiredDbFields.forEach( f =>{
            if( !baseQueryOptions.select.fields.includes(f) ) {
                baseQueryOptions.select.fields.push(f)
            }
        })
    }

    const selectFields = (baseQueryOptions.select && baseQueryOptions.select.fields && baseQueryOptions.select.fields.length > 0) ?
                         baseQueryOptions.select.fields.join(', ') : '*';
    const distinct = (baseQueryOptions.select.distinct) ? ' DISTINCT ' : '';
    let select = `SELECT ${distinct} ${selectFields}`
    let where = (baseQueryOptions.where) ? ' WHERE ' + baseQueryOptions.where : '';

    if (rqlQuery && !baseQueryOptions.ignoreRql) {
        // const where = req.url.split('where=')[1]
        let rqlOptions = convertRqlToSql(rqlQuery, baseQueryOptions, modelAdapter);
        dbQuery.params = dbQuery.params.concat(rqlOptions.values);
        if (rqlOptions.select) {
            requiredDbFields.forEach( f =>{
                if( !rqlOptions.select.includes(f) ){
                    rqlOptions.select.push(f)
                }
            })
            select = `SELECT ${distinct} ${rqlOptions.select.join(', ')} `
        }
        where = rqlOptions.where
    }

    dbQuery.sql = `${select} FROM ${baseQueryOptions.from} ${where}`

    if (baseQueryOptions.arraySize) {
        dbQuery.arraySize = baseQueryOptions.arraySize
    }

    // if(baseQueryOptions.sort && baseQueryOptions.sort.field){
    //     dbQuery.sql+=` ORDER BY ${modelAdapter.getDbAttribute(baseQueryOptions.sort.field)}`
    //     if(baseQueryOptions.sort.dir){
    //         dbQuery.sql+=` ${baseQueryOptions.sort.dir} `
    //     }
    // }
    // console.log(dbQuery)
    return dbQuery
    // return  db2Conn.querySync(db2Query)
}

export function dbTojsonData(data: any, modelAdapter: ModelAdapter) {
    if (data.length > 0) {
        data.forEach((item: any) => {
            Object.keys(item).forEach((attr: any) => {
                let jsonAttr = modelAdapter.getJsonAttribute(attr);
                if (jsonAttr) {
                    let o = item
                    let valueHandler = getHandler(modelAdapter.getJsonAttributeType(jsonAttr));
                    const jsonAttrObjStructure = jsonAttr.split('.')
                    if (jsonAttrObjStructure.length > 1) {
                        for (let i = 0; i < jsonAttrObjStructure.length - 1; i++) {
                            const n = jsonAttrObjStructure[i];
                            if (o[n] == null) {
                                o[n] = {}
                            }
                            o = o[n]
                        }
                        jsonAttr = jsonAttrObjStructure[jsonAttrObjStructure.length - 1]
                    }
                    let value = valueHandler.db2TojsonData(item[attr]);
                    if (typeof value === 'string') {
                        o[jsonAttr] = value.trim()
                    } else {
                        o[jsonAttr] = value
                    }
                    // else{
                    // }
                }
                delete item[attr]
            })
        })
    }
    return data
}

export function query(req: express.Request, db2QueryOptions: any){
    return new Promise((resolve: any, reject: any)=>{
        req.logger.debug('db2 query', db2QueryOptions)
        req.dbConnection.query(db2QueryOptions, function(err: any, rows: any[]){
            if(err){
                let errRes = {
                    code: StatusCodes.INTERNAL_SERVER_ERROR,
                    raw: err,
                    message: err.message
                }
                req.logger.error(err.message, {
                    last_sql: db2QueryOptions.sql,
                    last_sql_params: db2QueryOptions.params,
                    source: 'ibm_db',
                })
                reject(errRes)
            }else{
                resolve(rows)
            }
        })
    })
}

