import * as express from 'express'
const util = require('util')


const LogLevel =  {
    debug :'debug',
    info :'info',
    warning :'warning',
    error :'error',
    critical :'critical'
}

type LogMessage = {
    owner_team: string
    log_message: string
    log_level: string
    app_name: string
    app_route: string
    app_method: string
    app_query?: any
    app_route_params: string
    app_version: string
    x_ovs_correlation_id? : string,
    x_request_id? : string,
    user_id?: string
    last_sql?: string
    last_sql_params?: string
    app_elapsed_ms?: string
    app_staktrace?: string
}

export class OvsLogger {

    req: express.Request;
    ownerTeam: string;
    appName: string;
    appVersion: string;

    constructor(req: express.Request, ownerTeam: string, appName: string, appVersion: string) {
        this.req = req;
        this.ownerTeam = ownerTeam;
        this.appName = appName;
        this.appVersion = appVersion;
    }

    debug = (message: string, extraArgs?: object) => { this.log(LogLevel.debug, message, extraArgs) }
    info = (message: string, extraArgs?: object) => { this.log(LogLevel.info, message, extraArgs) }
    warning = (message: string, extraArgs?: object) => { this.log(LogLevel.warning, message, extraArgs) }
    error = (message: string, extraArgs?: object) => { this.log(LogLevel.error, message, extraArgs) }
    critical = (message: string, extraArgs?: object) => { this.log(LogLevel.critical, message, extraArgs) }

    log = function (logLevel: string, message: string, extraArgs?: object){

        if (!checkLogLevel(logLevel)){
            return
        }
        let log : LogMessage = {
            owner_team: this.ownerTeam,
            app_name: this.appName,
            app_version: this.appVersion,
            app_route:  this.req.originalUrl,
            app_method:  this.req.method,
            app_query:  this.req.query,
            app_route_params: JSON.stringify(this.req.params),
            log_level: logLevel,
            log_message: message,
        }

        let h = this.req.headers
        let corrId = h['x-ovs-correlation-id'];
        if(corrId){
            log.x_ovs_correlation_id = corrId;
            if(!Array.isArray(corrId)){
                let arr = corrId.split(';')
                if(arr.length>2){
                    log.user_id = arr[3]
                }
            }
        }

        log.x_request_id = h['x-request-id']

        let res = {
            body:{
                ...extraArgs,
                ...log
            }
        }

        if(process.env.DEV=='TRUE'){
            console.log(util.inspect(res, {showHidden: false, depth: null, colors: true}))
        }else{
            console.log(JSON.stringify(res))
        }


    }
}

const checkLogLevel = function(level: string){
    let appLevel = LogLevel.info
    if(process.env.LOG_LEVEL && Object.values(LogLevel).includes(process.env.LOG_LEVEL.toLowerCase())){
        appLevel = process.env.LOG_LEVEL.toLowerCase();
    }
    if(appLevel!=level && appLevel!=LogLevel.debug){
        if(appLevel==LogLevel.info && level==LogLevel.debug){
            return false
        }
        if(appLevel==LogLevel.warning && (level==LogLevel.debug || level==LogLevel.info) ){
            return false
        }
        if(appLevel==LogLevel.error && (level==LogLevel.debug || level==LogLevel.info || level==LogLevel.warning) ){
            return false
        }
        if(appLevel==LogLevel.critical && level!=LogLevel.critical){
            return false
        }
    }
    return true
}

declare global {
  namespace Express {
    interface Request {
      logger: OvsLogger
    }
  }
}