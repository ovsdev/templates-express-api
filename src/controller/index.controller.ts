import { jsonResponse, errorResponse, unimplementedResponse, okResponse } from '../utils/api-utils'
import * as express from 'express'
import IndexModel from '../model/index.model';

class IndexController{

    /**
     * @swagger
     * /index/{id}:
     *   get:
     *     description: Get index by id .
     *     summary: Get an Index element by his id.
     *     tags: [Index]
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: path
     *         name: id
     *         schema:
     *           type: number
     *         required: true
     *         description: ID parameter description
     *       - in: header
     *         name: Authorization
     *         type: string
     *         required: true
     *     responses:
     *       "200":
     *          description: Index details
     *          content:
     *              application/json:
     *                  schema:
     *                      $ref: '#/definitions/IndexStyle'
     *       "404":
     *         description: An Index with the specified ID was not found.
     *       "400":
     *         $ref: '#/definitions/BadRequestResponse'
     *       "500":
     *         $ref: '#/definitions/InternalServerErrorResponse'
     */
    static async getById(req: express.Request, res: express.Response) {
        try {
            let model = new IndexModel();
            let result = await model.getById(req)
            jsonResponse(req, res, result)
        } catch (error) {
            errorResponse(req, res, error)
        }
    }


    /**
     * @swagger
     * /index/{id}:
     *   patch:
     *     description: Patches an existing Index.
     *     summary: Patches an existing Index..
     *     tags: [Index]
     *     produces:
     *       - application/json
     *     parameters:
     *       - in: path
     *         name: id
     *         schema:
     *           type: number
     *         required: true
     *         description: Index id
     *       - in: header
     *         name: Authorization
     *         type: string
     *         required: true
     *       - in: body
     *         name: index
     *         description: Index body.
     *         schema:
     *              $ref: '#/definitions/IndexBody'
     *     responses:
     *       "200":
     *          description: Index successfully updated
     *       "404":
     *         description: An Index with the specified ID was not found.
     *       "400":
     *         $ref: '#/definitions/BadRequestResponse'
     *       "500":
     *         $ref: '#/definitions/InternalServerErrorResponse'
     */
     static async patch(req: express.Request, res: express.Response) {

        try {
            let model = new IndexModel();
            await model.patch(req)
            return okResponse(res)
        } catch (error) {
            errorResponse(req, res, error)
        }
    }
}


export default IndexController