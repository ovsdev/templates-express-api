import path = require("path");

export const swaggerSpecifications = {
    swaggerDefinition: {
        openapi: '3.0.0',
        info: {
            title: 'Template Express APIs',
            version: process.env.APP_VERSION,
        },
    },
    apis: [__dirname+'/controller/*.js', __dirname+'/model/*.js',__dirname+'/swagger/*.yaml'],
}

export const swaggerOptions={
  customSiteTitle: "OVS Template Express Api",
  customfavIcon: path.resolve(__dirname, "./public/favicon.ico")
}

export const connStr = `DATABASE=${process.env.DB_NAME};HOSTNAME=${process.env.DB_HOSTNAME};UID=${process.env.DB_USERNAME};PWD=${process.env.DB_PASSWORD};PORT=${process.env.DB_PORT};PROTOCOL=${process.env.DB_PROTOCOL}`;

