import dotenv from "dotenv";
import path from "path";
dotenv.config({ path: __dirname + '/.env' });

import express from "express";
import cookieParser from "cookie-parser";
import logger from "morgan";
import swaggerJsdoc from "swagger-jsdoc";
import swaggerUi from "swagger-ui-express";
import { errorResponse , logRequest, OvsError } from './utils/api-utils';
import { indexRouter } from './routes/index';
const ibmdb = require("ibm_db")
import { OvsLogger } from './utils/ovs-logger'

import { swaggerOptions, swaggerSpecifications ,connStr} from './constants';
const app = express();

app.use(logger('tiny'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const swaggerSpec: any = swaggerJsdoc(swaggerSpecifications);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerSpec, swaggerOptions));

app.use((req, res, next) => {
    req.logger = new OvsLogger(req, process.env.OWNER_TEAM, process.env.APP_NAME, process.env.APP_VERSION);
    ibmdb.open(connStr, async (err: any, connection: any) => {
        if (err) {
            console.log(err);
            errorResponse(req, res, {message: 'Cannot open connection to database'});
            return;
        }
        req.dbConnection = connection;
        res.on('close', ()=>{
            connection.close();
        })
        next();
    });
});


app.use('/indexes', logRequest, indexRouter(swaggerSpec));

// start the Express server
const port = process.env.CONTAINER_PORT || 3000;
app.listen(port, () => {
    console.log(`server started at http://localhost:${port}`);
});

module.exports = app;
