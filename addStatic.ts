import * as shell from "shelljs";

shell.cp( "-R", "src/public", "dist/" );
shell.cp( "-R", "src/swagger", "dist/" );
shell.cp( "src/.env", "dist/.env" );