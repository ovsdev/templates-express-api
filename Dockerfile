FROM node:12-slim

RUN npm install -g typescript tslint 

RUN apt-get update && apt-get install make python build-essential libxml2 -y

WORKDIR /usr/app

copy . .

RUN npm install

RUN npm run build

EXPOSE 3000

CMD [ "npm", "start" ]